# btrfsbackup
# Copyright (C) 2017 Max Harmathy
#
# This program is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, see <http://www.gnu.org/licenses/>.


import argparse
import datetime
import json
import os
import re
import subprocess
import sys


class Logging:

    @staticmethod
    def log(*message_values):
        print(*message_values)

    @staticmethod
    def error(*message_values):
        print("Error:", *message_values, file=sys.stderr)


class Configuration(object):
    DEFINED_PARAMETERS = [
        "system_volumes_path",
        "backup_media_path",
        "snapshots_directory",
    ]

    def __init__(self):
        for option in self.DEFINED_PARAMETERS:
            self.__setattr__(option, None)
            self.dry_run = False

    def load(self, file):
        with open(file, 'r') as handle:
            data = json.load(handle)
            for option in self.DEFINED_PARAMETERS:
                if option in data:
                    self.__setattr__(option, data[option])


class AbstractPath(object):

    def __init__(self, path):
        self.path = path
        self._subvolumes = None
        self._existence_known = False
        self._existence_status = False

    def get_subvolumes(self):
        """Return a list of btrfs subvolumes located in directly in this directory"""
        if not self._subvolumes:
            self._subvolumes = []
            if self.exists():
                for file in os.listdir(self.path):
                    volume = Volume(os.path.join(self.path, file))
                    if volume.sanity_check():
                        self._subvolumes.append(volume)
        return self._subvolumes

    def invalidate_subvolume_cache(self):
        self._subvolumes = None

    def __eq__(self, other):
        return isinstance(other, AbstractPath) and self.path == other.path

    def get_subpath(self, directory):
        return AbstractPath(os.path.join(self.path, directory))

    def exists(self):
        if not self._existence_known:
            self._existence_status = os.path.exists(self.path)
            self._existence_known = True
        return self._existence_status

    def ensure_exists(self):
        if not self.exists():
            os.mkdir(self.path)
            self._existence_known = False


class Volume(AbstractPath):

    def __init__(self, path):
        super().__init__(path)
        self.name = os.path.basename(path)
        self.pattern = re.compile("(.+)-(\\d{4}-\\d{2}-\\d{2})")

    def sanity_check(self):
        return self.check_filesystem() and self.check_volume_root() and self.check_cow_enabled()

    def check_filesystem(self):
        """Check if we are really on a btrfs filesystem"""
        command = ["stat", "-f", "--format=%T", self.path]
        result = subprocess.check_output(command)
        return result.decode().strip() == "btrfs"

    def check_volume_root(self):
        """Check if we are really on a btrfs volume root"""
        command = ["stat", "--format=%i", self.path]
        result = subprocess.check_output(command)
        return result.decode().strip() == "256"

    def check_cow_enabled(self):
        command = ["lsattr", "-d", self.path]
        result = subprocess.check_output(command)
        return "C" not in [chr(element) for element in result]

    def new_snapshot(self, target_directory):
        """
        Generate a Snapshot object for today.
        Note that this does not include creating the physical snapshot.
        :param target_directory: Where to put the snapshot
        :return: the Snapshot object
        """
        today = datetime.date.today()
        path = os.path.join(target_directory.path, self.name + "-" + today.isoformat())
        return Snapshot(path, self, today)

    def existing_snapshots(self, target_directory):
        """
        Generate a list of snapshots of this volume in the target directory.
        :type target_directory: AbstractPath
        :return: List of snapshots 
        """
        result = []
        for volume in target_directory.get_subvolumes():
            try:
                match = self.pattern.match(volume.name)
                if match:
                    base_name, date = match.groups()
                    if base_name == self.name:
                        snapshot = Snapshot(volume.path, self, datetime.datetime.strptime(date, "%Y-%m-%d"))
                        result.append(snapshot)
            except ValueError:
                pass
        return result


class Snapshot(Volume):

    def __init__(self, path, base_volume, date):
        super().__init__(path)
        self.base_volume = base_volume
        self.date = date
        self._directory = AbstractPath(os.path.dirname(self.path))

    def is_snapshot_of(self, volume):
        return self.base_volume == volume

    def commit(self):
        self._directory.ensure_exists()
        command = [
            "btrfs",
            "subvolume",
            "snapshot",
            "-r",
            self.base_volume.path,
            self.path,
        ]
        subprocess.run(command)

    def transfer(self, target_directory, base_snapshot=None):
        target_directory.ensure_exists()
        send_command = [
            "btrfs",
            "send",
            self.path,
        ]
        if base_snapshot:
            send_command.insert(-1, "-p")
            send_command.insert(-1, base_snapshot.path)

        receive_command = [
            "btrfs",
            "receive",

            target_directory.path,
        ]

        send_process = subprocess.Popen(send_command, stdout=subprocess.PIPE)
        receive_process = subprocess.Popen(receive_command, stdin=send_process.stdout)
        # TODO handle stderr
        send_process.wait()
        receive_process.wait()


class BackupTask(object):

    def __init__(self, snapshot, target_directory, base_snapshot=None):
        self.snapshot = snapshot
        self.target_directory = target_directory
        self.base_snapshot = base_snapshot
        self._repr = None

    def __repr__(self):
        if not self._repr:
            self._repr = "Backup task: create a snapshot of {0} as {1} and trasfer to {3}{2}".format(
                self.snapshot.base_volume.path,
                self.snapshot.name,
                " incremental on basis of previous backup " + self.base_snapshot.name if self.base_snapshot else "",
                self.target_directory.path,
            )
        return self._repr


class Backup(object):

    def __init__(self, configuration):
        self.configuration = configuration
        self.system_base_path = AbstractPath(self.configuration.system_volumes_path)
        self._tasks = []

    def ensure_sane_configuration(self):
        if not os.path.exists(self.configuration.backup_media_path):
            raise IOError(self.configuration.backup_media_path + " does not exist!")

    def create_backup_plan(self):
        system_volumes = self.system_base_path.get_subvolumes()
        for system_volume in system_volumes:
            self.add_volumes_from(system_volume)

    def add_volumes_from(self, system_volume):
        snapshot_directory = system_volume.get_subpath(self.configuration.snapshots_directory)
        backup_target_directory = Volume(os.path.join(self.configuration.backup_media_path, system_volume.name))
        existing_backups = [volume.name for volume in backup_target_directory.get_subvolumes()]
        for source_volume in system_volume.get_subvolumes():
            base_snapshot = None
            for base_snapshot_candidate in source_volume.existing_snapshots(snapshot_directory):
                if base_snapshot_candidate.is_snapshot_of(source_volume):
                    if base_snapshot_candidate.name in existing_backups:
                        if not base_snapshot or base_snapshot.date < base_snapshot_candidate.date:
                            base_snapshot = base_snapshot_candidate
            new_snapshot = source_volume.new_snapshot(snapshot_directory)
            self._tasks.append(BackupTask(new_snapshot, backup_target_directory, base_snapshot))

    def execute(self):
        self.create_backup_plan()
        for task in self._tasks:
            Logging.log(task)
        if self.configuration.dry_run:
            return
        for task in self._tasks:
            task.snapshot.commit()
        for task in self._tasks:
            task.snapshot.transfer(task.target_directory, task.base_snapshot)


class Cli(object):

    def __init__(self):
        self.configuration = None

    @staticmethod
    def parse_arguments():
        parser = argparse.ArgumentParser(description="incremantal backup using btrfs subvolumes")
        parser.add_argument("-b", "--system-volumes-path", help="base path for system volumes")
        parser.add_argument("-m", "--backup-media-path", help="path to the backup medium")
        parser.add_argument("-s", "--snapshots-directory", help="directory in volume to store snapshots")
        parser.add_argument("-c", "--config-file", help="load parameter from configuration file")
        parser.add_argument("-n", "--dry-run", action='store_true', help="list only what would be backed up")

        args = parser.parse_args()
        config = Configuration()

        if args.config_file:
            config.load(args.config_file)

        if args.backup_media_path:
            config.backup_media_path = args.backup_media_path

        if args.system_volumes_path:
            config.system_volumes_path = args.system_volumes_path

        if args.snapshots_directory:  # TODO add default value
            config.snapshots_directory = args.snapshots_directory

        config.dry_run = args.dry_run

        return config

    @staticmethod
    def fail(*message_values, exit_code=1):
        Logging.error(*message_values)
        exit(exit_code)

    def run(self):
        self.configuration = Cli.parse_arguments()
        missing_option = False
        for opt in Configuration.DEFINED_PARAMETERS:
            value = self.configuration.__getattribute__(opt)
            if not value:
                Logging.error("Parameter", opt, "not set")
                missing_option = True
            else:
                Logging.log("Parameter", opt, "=", value)
        if missing_option:
            Cli.fail("Required parameters are missing!", exit_code=2)
        backup = Backup(self.configuration)
        try:
            backup.ensure_sane_configuration()
            backup.execute()
        except IOError as e:
            Cli.fail(str(e))
        except PermissionError as e:
            Cli.fail(e.strerror, e.filename)
